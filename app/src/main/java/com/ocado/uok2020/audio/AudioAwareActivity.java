package com.ocado.uok2020.audio;

import androidx.appcompat.app.AppCompatActivity;

public class AudioAwareActivity extends AppCompatActivity {

    private SfxPlayer sfxPlayer = new SfxPlayer();

    public SfxPlayer getAudioPlayer() {
        return sfxPlayer;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sfxPlayer.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sfxPlayer.init(this);
    }
}
