package com.ocado.uok2020.audio;

import android.content.Context;
import android.media.SoundPool;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;


import com.ocado.uok2020.R;

import static android.media.AudioManager.STREAM_MUSIC;

public class SfxPlayer {
    private static final String TAG = "SFX-Player";

    private static final int MAX_STREAMS = 1;
    private static final int PRIORITY_ONE = 1;
    private static final int NO_LOOP = 0;
    private static final float FULL_VOLUME = 1.0f;
    private static final float NORMAL_SPEED = 1.0f;

    private SoundPool preemptingSoundPool;
    private SparseIntArray soundsLoaded;
    private SparseBooleanArray soundsReady;

    SfxPlayer() {
        soundsLoaded = new SparseIntArray();
        soundsReady = new SparseBooleanArray();
    }

    public int play(int soundId) {
        Log.i(TAG, String.format("Playback of sound id: %d requested", soundId));
        if (isReadyToPlay(soundId)) {
            return preemptingSoundPool.play(soundsLoaded.get(soundId), FULL_VOLUME, FULL_VOLUME, PRIORITY_ONE, NO_LOOP, NORMAL_SPEED);
        }
        Log.w(TAG, String.format("Sound id: %d not ready to play", soundId));
        return -1;
    }

    void init(Context context) {
        Log.d(TAG, "Initializing SFX Player...");
        preemptingSoundPool = buildPreemptiveSoundPool(context);
        Log.d(TAG, "SFX Player initialized");
    }

    void destroy() {
        Log.d(TAG, "Destroying SFX Player...");
        preemptingSoundPool.release();
        preemptingSoundPool = null;
        soundsLoaded.clear();
        soundsReady.clear();
        Log.d(TAG, "SFX Player destroyed");
    }

    private SoundPool buildPreemptiveSoundPool(Context context) {
        SoundPool soundPool = new SoundPool(MAX_STREAMS, STREAM_MUSIC, 0);

        setupLoadCompleteListener(soundPool);
        loadAllSfx(context, soundPool);

        return soundPool;
    }

    private void setupLoadCompleteListener(SoundPool soundPool) {
        soundPool.setOnLoadCompleteListener(
                (soundPool1, sampleId, status) -> soundsReady.put(sampleId, Boolean.TRUE)
        );
    }

    private void loadAllSfx(Context context, SoundPool soundPool) {
        loadSfx(context, soundPool, R.raw.confirmation);
    }

    private void loadSfx(Context context, SoundPool soundPool, int soundId) {
        int streamId = soundPool.load(context, soundId, PRIORITY_ONE);
        soundsLoaded.put(soundId, streamId);
    }

    private boolean isReadyToPlay(int soundId) {
        return isSoundLoaded(soundId) && isSoundReady(soundId);
    }

    private boolean isSoundReady(int soundId) {
        return soundsReady.get(soundsLoaded.get(soundId));
    }

    private boolean isSoundLoaded(int soundId) {
        return soundsLoaded.get(soundId) != 0;
    }
}
